window.onload= getstatsgen();

function getstatsgen(){
fetch('http://localhost:5000/statsgen')
.then(r => {
   return r.json()
 })
.then(r => {
console.log(r)

const data1 = {
    labels: ['Blanc', 'Bleu', 'Noir', 'Rouge', 'Vert'],
    datasets: [
      {
        label: 'couleurs',
        data: [r[0][0][1], r[0][1][1], r[0][2][1], r[0][3][1], r[0][4][1]],
          backgroundColor: [
            'rgba(255, 255, 255, 0.6)',
            'rgba(33, 23, 125, 0.3)',
            'rgba(0, 0, 0, 0.3)',
            'rgba(198, 8, 0, 0.3)',
            'rgba(20, 148, 20, 0.3)'
          ],
      }
    ]
  };

  const data2 = {
    labels: ["Commune","Peu commune", "Rare", "Mythique"],
    datasets: [
      {
        label: "raretés",
        data: [r[2][1][1], r[2][4][1], r[2][3][1], r[2][2][1]],
        borderColor: 'rgba(20, 148, 20, 1)',
        backgroundColor: ['rgba(0, 0, 0, 0.6)',
        'rgba(206, 206, 206, 0.6)',
        'rgba(255, 215, 0, 0.6)',
        'rgba(235, 0, 0, 0.6)'],
      }
    ]
  };

  const data3 = {
    labels: ["Créatures","Rituels", "Ephémères", "Enchantements","Artefacts", "Terrains"],
    datasets: [
      {
        label: "types de cartes",
        data: [r[3][0][1], r[3][1][1], r[3][2][1], r[3][3][1], r[3][4][1], r[3][5][1]],
        //borderColor: 'rgba(250, 0, 0, 1)',
        backgroundColor: ['rgba(0, 0, 0, 0.6)',
        'rgba(255, 255, 255, 0.6)',
        'rgba(255, 255, 255, 0.6)',
        'rgba(255, 255, 255, 0.6)',
        'rgba(255, 255, 255, 0.6)',
        'rgba(255, 255, 255, 0.6)'],
      }
    ]
  };


  const data4 = {
    labels: ["Simic","Dimir", "Azorius", "Orzhov","Izzet", "Rakdos", "Golgari","Gruul", "Boros", "Selesnya"],
    datasets: [
      {
        label: "guildes",
        data: [r[5][0][1], r[5][1][1], r[5][2][1], r[5][3][1], r[5][4][1], r[5][5][1],r[5][6][1],r[5][7][1],r[5][8][1],r[5][9][1]],
        //borderColor: 'rgba(250, 0, 0, 1)',
        backgroundColor:'rgba(0, 0, 0, 0.6)',
      }
    ]
  };

  const config1 = {
    type: 'polarArea',
    data: data1,
    options: {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Collection par couleurs'
        }
      }
    },
  };

  const config2 = {
    type: 'bar',
    data: data2,
    options: {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Répartition des raretés'
        }
      }
    },
  };

  const config3 = {
    type: 'radar',
    data: data3,
    options: {
      responsive: true,
      plugins: {
        title: {
          display: true,
          text: 'Collection par types'
        }
      }
    },
  };

  const config4 = {
    type: 'bar',
    data: data4,
    options: {
      elements: {
      },
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Collection et guildes'
        }
      }
    },
  };


  var myChart1 = new Chart(
    document.getElementById('colors'),
    config1
  );


  var myChart2 = new Chart(
    document.getElementById('rarity'),
    config2
  );

  var myChart3 = new Chart(
    document.getElementById('types'),
    config3
  );

  var myChart4 = new Chart(
    document.getElementById('guildes'),
    config4
  );
})
}
   
   
   