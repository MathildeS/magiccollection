""" # compose_flask/app.py
from flask import Flask
from redis import Redis

app = Flask(__name__)
redis = Redis(host='redis', port=6379)

@app.route('/')

def hello():
    redis.incr('hits')
    return 'This Compose/Flask demo has been viewed %s time(s).' % redis.get('hits')


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
 """

from flask import Flask
from flask import request
from flask import render_template
from flask import send_from_directory
from flask import jsonify

from mtgsdk import Card
from mtgsdk import Set
from mtgsdk import Type
from mtgsdk import Supertype
from mtgsdk import Subtype
from mtgsdk import Changelog

import scrython

import time
import asyncio
import aiohttp
import pandas as pd
import psycopg2
import datetime
from pathlib import Path
from pathlib import PurePath
import json

app = Flask(__name__)


@app.route('/')
def index():

    #définition des variables associées à la connexion à PostgreSQL
    db_name = 'postgres' #nom de la base de données
    user = 'postgres' # nom de l'utilisateur
    host = 'postgis' # nom du host
    port = 5432 # numéro de port
    password="password"

    #connexion a la base de données 'freqrando_exemple'
    try:
        con = psycopg2.connect(

        host= host,  #indiquer le nom du host
        database= db_name,#indiquer le nom de la base de données
        password= password,
        user= user,#indiquer le nom de l'utilisateur
        port= port  #le numéro de port
        ) 

    except:
        print("I am unable to connect to the database")

    cursor = con.cursor()

    cartes_query="SELECT SUM(exemplaires) FROM mes_cartes;"
    cursor.execute(cartes_query)
    nbr= cursor.fetchmany(2)[0][0]



    last_carte_query="SELECT nom, image_url, flavor FROM mes_cartes ORDER BY RANDOM() LIMIT 1;"
    cursor.execute(last_carte_query)

    result=cursor.fetchmany(2)
    last=result[0][0]
    url=result[0][1]
    flav=result[0][2]

    randomflavor_query="SELECT flavor, nom FROM mes_cartes WHERE flavor <> 'None' ORDER BY RANDOM() LIMIT 1 ;"
    cursor.execute(randomflavor_query)
    result2=cursor.fetchmany(2)
    print(result2)
    flav=result2[0][0]
    nomflav=result2[0][1]

    con.commit()
    cursor.close()
    con.close()

    return render_template("index.html", nbrc=nbr, lastc=last, urlc=url, flavor=flav, flavorn=nomflav)


@app.route('/visio')
def visio():
    return render_template("extvisio.html")

#renvoie image d'une carte
@app.route('/carte/<id>')
def cartecho(id):
    url= Card.find(id).image_url
    return f'<img src={url}>'

#renvoie images d'un set
@app.route('/set/<code>',methods=["GET"])
def setcho(code):
    liste= Card.where(set=code).all()
    text=''
    for i in liste:
        text+=f'<img src={i.image_url}>'
    return text

#renvoie les infos pour la recherche avancée
@app.route('/rechav/<criteres>',methods=["GET"])
def rechav(criteres):
    #définition des variables associées à la connexion à PostgreSQL
    db_name = 'postgres' #nom de la base de données
    user = 'postgres' # nom de l'utilisateur
    host = 'postgis' # nom du host
    port = 5432 # numéro de port
    password="password"

    #connexion a la base de données 'freqrando_exemple'
    con = psycopg2.connect(

        host= host,  #indiquer le nom du host
        database= db_name,#indiquer le nom de la base de données
        user= user,#indiquer le nom de l'utilisateur
        port= port,  #le numéro de port,
        password=password
    )

    cursor = con.cursor()

    criteres=json.loads(criteres)

    if criteres["rarete"]=="Tout":
        criteres["rarete"]="1"
        raritysearch = "1=%s"
    else: 
        raritysearch = "rarity=%s"

    if criteres["set"]=="Tout":
        criteres["set"]="1"
        setsearch = "1=%s"
    else: 
        setsearch = "set_name=%s"

    if criteres["typ"]=="Tout":
        typsearch = "%"+""+"%"
    else: 
        typsearch = "%"+str(criteres["typ"])+"%"

    search_Query= 'select nom, image_url, exemplaires,typ,texte, setcode, numero  from mes_cartes where '+raritysearch+' and '+setsearch+' and nom like %s and typ like %s and texte like %s'

    nom= "%"+str(criteres["nom"])+"%"
    texte= "%"+str(criteres["texte"])+"%"
    
    cursor.execute(search_Query,(str(criteres["rarete"]),str(criteres["set"]),nom, typsearch,texte))
    result= cursor.fetchall()
     
    result=json.dumps([result])

    return f"{result}"

#renvoie les infos pour la recherche avancée
@app.route('/rechavlist',methods=["GET"])
def rechavlist():
    #définition des variables associées à la connexion à PostgreSQL
    db_name = 'postgres' #nom de la base de données
    user = 'postgres' # nom de l'utilisateur
    host = 'postgis' # nom du host
    port = 5432 # numéro de port
    password="password"

    #connexion a la base de données 'freqrando_exemple'
    con = psycopg2.connect(

        host= host,  #indiquer le nom du host
        database= db_name,#indiquer le nom de la base de données
        user= user,#indiquer le nom de l'utilisateur
        port= port,  #le numéro de port,
        password=password
    )

    cursor = con.cursor()

    get_types='select distinct typ from mes_cartes order by typ ASC;'
    cursor.execute(get_types)
    typecarte= cursor.fetchall()

    newtypecarte=[]
    helpl=[]
    for i in range(len(typecarte)):
        if typecarte[i][0].find("\u2014") >0:
            decomp=typecarte[i][0].split('\u2014')
            decomp[1]=decomp[1].split()
            helpl.append(decomp[1])
            for j in range(len(decomp[1])):
                newtypecarte.append(str(decomp[1][j]))
        else:
            newtypecarte.append(str(typecarte[i][0]))

    newtypecarte=sorted(list(set(newtypecarte)))

    get_setnames='select distinct set_name from mes_cartes order by set_name ASC;'
    cursor.execute(get_setnames)
    setnames= cursor.fetchall()

    newsetnames=[]
    for i in range(len(setnames)):
        newsetnames.append(str(setnames[i][0]))

    newsetnames=sorted(list(set(newsetnames)))
     
    criteres=json.dumps([newtypecarte, newsetnames, typecarte, helpl])

    return f"{criteres}"

#renvoie les stats de couleur
@app.route('/statsgen',methods=["GET"])
def couleurs():
    #définition des variables associées à la connexion à PostgreSQL
    db_name = 'postgres' #nom de la base de données
    user = 'postgres' # nom de l'utilisateur
    host = 'postgis' # nom du host
    port = 5432 # numéro de port
    password="password"

    #connexion a la base de données 'freqrando_exemple'
    con = psycopg2.connect(

        host= host,  #indiquer le nom du host
        database= db_name,#indiquer le nom de la base de données
        user= user,#indiquer le nom de l'utilisateur
        port= port,  #le numéro de port,
        password=password
    )

    cursor = con.cursor()

    get_colors='select colors, sum(exemplaires) from mes_cartes group by colors;'
    cursor.execute(get_colors)
    couleurs= cursor.fetchall()

    white=0
    blue=0
    black=0
    red=0
    green=0

    izzet=0
    simic=0
    golgari=0
    azorius=0
    boros=0
    rakdos=0
    dimir=0
    orzhov=0
    gruul=0
    selesnya=0

    for i in range(len(couleurs)):
        if str(couleurs[i][0]).find('White')> 0:
            white+= int(couleurs[i][1])
        if str(couleurs[i][0]).find('Blue')> 0:
            blue+= int(couleurs[i][1])
        if str(couleurs[i][0]).find('Black')> 0:
            black+= int(couleurs[i][1])
        if str(couleurs[i][0]).find('Red')> 0:
            red+= int(couleurs[i][1])
        if str(couleurs[i][0]).find('Green')> 0:
            green+= int(couleurs[i][1])

        if str(couleurs[i][0]).find('Green')> 0 and str(couleurs[i][0]).find('Blue')> 0 and str(couleurs[i][0]).find('Red')< 0 and str(couleurs[i][0]).find('Black')< 0 and str(couleurs[i][0]).find('White')< 0:
            simic+=int(couleurs[i][1])
        if str(couleurs[i][0]).find('Blue')> 0 and str(couleurs[i][0]).find('Black')> 0 and str(couleurs[i][0]).find('Red')< 0 and str(couleurs[i][0]).find('Green')< 0 and str(couleurs[i][0]).find('White')< 0:
            dimir+=int(couleurs[i][1])
        if str(couleurs[i][0]).find('Blue')> 0 and str(couleurs[i][0]).find('White')> 0 and str(couleurs[i][0]).find('Red')< 0 and str(couleurs[i][0]).find('Green')< 0 and str(couleurs[i][0]).find('Black')< 0:
            azorius+=int(couleurs[i][1])
        if str(couleurs[i][0]).find('Black')> 0 and str(couleurs[i][0]).find('White')> 0 and str(couleurs[i][0]).find('Red')< 0 and str(couleurs[i][0]).find('Green')< 0 and str(couleurs[i][0]).find('Blue')< 0:
            orzhov+=int(couleurs[i][1])
        if str(couleurs[i][0]).find('Red')> 0 and str(couleurs[i][0]).find('Blue')> 0 and str(couleurs[i][0]).find('Black')< 0 and str(couleurs[i][0]).find('Green')< 0 and str(couleurs[i][0]).find('White')< 0:
            izzet+=int(couleurs[i][1])
        if str(couleurs[i][0]).find('Red')> 0 and str(couleurs[i][0]).find('Black')> 0 and str(couleurs[i][0]).find('Blue')< 0 and str(couleurs[i][0]).find('Green')< 0 and str(couleurs[i][0]).find('White')< 0:
            rakdos+=int(couleurs[i][1])
        if str(couleurs[i][0]).find('Green')> 0 and str(couleurs[i][0]).find('Black')> 0 and str(couleurs[i][0]).find('Blue')< 0 and str(couleurs[i][0]).find('Red')< 0 and str(couleurs[i][0]).find('White')< 0:
            golgari+=int(couleurs[i][1])
        if str(couleurs[i][0]).find('Green')> 0 and str(couleurs[i][0]).find('Red')> 0 and str(couleurs[i][0]).find('Blue')< 0 and str(couleurs[i][0]).find('Black')< 0 and str(couleurs[i][0]).find('White')< 0:
            gruul+=int(couleurs[i][1])
        if str(couleurs[i][0]).find('White')> 0 and str(couleurs[i][0]).find('Red')> 0 and str(couleurs[i][0]).find('Blue')< 0 and str(couleurs[i][0]).find('Black')< 0 and str(couleurs[i][0]).find('Green')< 0:
            boros+=int(couleurs[i][1])
        if str(couleurs[i][0]).find('White')> 0 and str(couleurs[i][0]).find('Green')> 0 and str(couleurs[i][0]).find('Blue')< 0 and str(couleurs[i][0]).find('Black')< 0 and str(couleurs[i][0]).find('Red')< 0:
            selesnya+=int(couleurs[i][1])

    couleursall =[["blanc",white],["bleu",blue],["noir",black],["rouge",red],["vert",green]]
    guildes = [["simic", simic],["dimir", dimir],["azorius", azorius],["orzhov", orzhov],["izzet", izzet],["rakdos", rakdos],["golgari", golgari],["gruul", gruul],["boros", boros],["selesnya", selesnya]]
    
    get_types='select typ, sum(exemplaires) from mes_cartes group by typ;'
    cursor.execute(get_types)
    types= cursor.fetchall()
    creatures=0
    sorceries=0
    instants=0
    artifacts=0
    enchantments=0
    lands=0
    help=[]
    for i in range(len(types)):
        if str(types[i][0]).find('Creature')> -1:
            creatures+= int(types[i][1])
        if str(types[i][0]).find('Sorcery')> -1:
            sorceries+= int(types[i][1])
        if str(types[i][0]).find('Instant')> -1:
            instants+= int(types[i][1])
        if str(types[i][0]).find('Enchantment')> -1:
            enchantments+= int(types[i][1])
        if str(types[i][0]).find('Land')> -1:
            lands+= int(types[i][1])
        if str(types[i][0]).find('Artifact')> -1:
            artifacts+= int(types[i][1])
    typesfin=[["créatures",creatures],["rituels",sorceries],["éphémères",instants], ["enchantements",enchantments], ["artefacts",artifacts],["terrains", lands]]

    get_rarity='select rarity, sum(exemplaires) from mes_cartes group by rarity;'
    cursor.execute(get_rarity)
    rarity= cursor.fetchall()

    get_nbrext='select count(distinct setcode) from mes_cartes;'
    cursor.execute(get_nbrext)
    nbrext= cursor.fetchall()

    datafin=json.dumps([couleursall,types,rarity, typesfin, nbrext, guildes])

    return f"{datafin}"

#renvoie informations des cartes correspondant aux critères
@app.route('/advancedsearch/<param>',methods=["GET"])
def advsearch(param):
    param["set"]
    param["color"]


@app.route('/getset', methods=["GET"])
def getset():

    listset=Set.where(type='expansion').all()
    listsetfin=[]
    for i in listset:
        listsetfin.append((i.name,i.code))
    listsetfin=json.dumps(listsetfin)
    return f"{listsetfin}"


#ajoute une carte dans la bdd
@app.route('/ajout/<data>', methods=['GET'])
def ajoutcarte(data):
    #définition des variables associées à la connexion à PostgreSQL
    db_name = 'postgres' #nom de la base de données
    user = 'postgres' # nom de l'utilisateur
    host = 'postgis' # nom du host
    port = 5432 # numéro de port
    password="password"

    #connexion a la base de données 'freqrando_exemple'
    con = psycopg2.connect(

        host= host,  #indiquer le nom du host
        database= db_name,#indiquer le nom de la base de données
        user= user,#indiquer le nom de l'utilisateur
        port= port,  #le numéro de port,
        password=password
    )

    cursor = con.cursor()

    #{"set":"THB","number":221}
    data=json.loads(data)
    card_to_add = Card.where(set=data["set"]).where(number=data["number"]).all()[0]

    check_Query='Select * from mes_cartes where setcode= %s and numero= %s;'
    cursor.execute(check_Query,(data["set"],str(data["number"])))

    if cursor.fetchmany(2)!=[]:
        Update_Query= 'Update mes_cartes Set exemplaires = exemplaires +1 where setcode= %s and numero= %s; '
        cursor.execute(Update_Query,(data["set"],str(data["number"])))
        con.commit()
        return f"Un nouvel exemplaire de '{card_to_add.name}' a été ajouté !"

    else:
        Add_Query= 'INSERT INTO mes_cartes (exemplaires,nom,multiverse_id,layout,namess,mana_cost,cmc,colors,color_identity,typ,supertypes,subtypes,rarity,texte,flavor,artist,numero,forces,toughness,loyalty,image_url,setcode,set_name,idid) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'

        cursor.execute(Add_Query,(1,card_to_add.name,card_to_add.multiverse_id,card_to_add.layout,card_to_add.names,card_to_add.mana_cost,card_to_add.cmc,card_to_add.colors,card_to_add.color_identity,card_to_add.type,card_to_add.supertypes,card_to_add.subtypes,card_to_add.rarity,card_to_add.text,card_to_add.flavor,card_to_add.artist,card_to_add.number,card_to_add.power,card_to_add.toughness,card_to_add.loyalty,card_to_add.image_url,card_to_add.set,card_to_add.set_name,card_to_add.id))

        con.commit()
        return f"La carte '{card_to_add.name}' a bien été ajoutée !"

    cursor.close()
    con.close()


if __name__ == '__main__':
    #app.debug= True
    app.run(host='0.0.0.0', port=5000)
